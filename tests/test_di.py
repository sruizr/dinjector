from unittest.mock import mock_open, patch

from di import ConfigService, DependencyInjector


class Artifact1:
    def __init__(self, par_a: str, par_b: str):
        self.par_a = par_a
        self.par_b = par_b


class Artifact2:
    def __init__(self, par_1: str, par_2: str):
        self.par_1 = par_1
        self.par_2 = par_2


class StubConfigService(ConfigService):
    def __init__(self, content):
        self._content = content

    def get(self, key: str, default=None):
        return self._content.get(key, default)


yaml_dependencies_content = """
artifact_1:
  module: test_di
  class: Artifact1
  args:
    - $key_a
    - $key_b
artifact_2:
  module: test_di
  class: Artifact2
  kwargs:
    par_1: \">artifact_1\"
    par_2: $key_2
artifact_3:
  module: test_di
  class: Artifact1
  args: $key-args
artifact_4:
  module: test_di
  class: Artifact2
  kwargs: $key-kwargs
artifact_5:
  module: test_di
  class: Artifact2
  kwargs:
    par_1: $key_a
    par_2: $key-kwargs
artifact_6:
  module: test_di
  class: Artifact2
  kwargs:
    par_1: 1
    par_2: direct-value
artifact_7:
  module: test_di
  class: Artifact1
  args:
    - 1
    - direct-value
artifact_8:
  module: test_di
  class: Artifact2
  kwargs:
    par_1: \"+artifact_1\"
    par_2: $key_2
"""


class A_DependencyInjector:
    def setup_method(self):
        self._open_patch = patch(
            "builtins.open", mock_open(read_data=yaml_dependencies_content)
        )
        self.open = self._open_patch.start()

    def teardown_method(self):
        self._open_patch.stop()

    def given_a_dependency_injector(
        self, config_content: dict | None = None, dependencies: dict | None = None
    ):
        default_config_content = {
            "key_a": "value-a",
            "key_b": "value-b",
            "key_2": "value-2",
            "key-args": ["key_a", "key_b"],
            "key-kwargs": {"par_1": "key_a", "par_2": "key_b"},
        }

        config_content = config_content if config_content else default_config_content
        self.config = StubConfigService(config_content)
        injector = DependencyInjector(self.config, "dependencies.yaml", "tests")
        self.open.assert_called_with("dependencies.yaml", "r")
        return injector

    def should_create_a_object_from_config_parameters(self):
        injector = self.given_a_dependency_injector()

        artifact = injector.construct("artifact_1")

        assert artifact.par_a == "value-a"
        assert artifact.par_b == "value-b"

    def should_be_implicit_singleton(self):
        injector = self.given_a_dependency_injector()

        assert injector["artifact_5"] == injector["artifact_5"]
        assert injector["artifact_5"] != injector.construct("artifact_5")

    def should_return_diferent_object_when_constructing(self):
        injector = self.given_a_dependency_injector()

        assert injector.construct("artifact_1") != injector.construct("artifact_1")

    def should_inject_list_as_args(self):
        injector = self.given_a_dependency_injector()

        artifact_3 = injector.construct("artifact_3")
        assert artifact_3.par_a == "key_a"
        assert artifact_3.par_b == "key_b"

    def should_inject_kwargs_components(self):
        injector = self.given_a_dependency_injector()

        artifact_5 = injector.construct("artifact_5")
        assert artifact_5.par_1 == "value-a"
        assert artifact_5.par_2 == {"par_1": "key_a", "par_2": "key_b"}

    def should_inject_direct_values_in_kwargs(self):
        injector = self.given_a_dependency_injector()

        artifact_6 = injector.construct("artifact_6")
        assert artifact_6.par_1 == 1
        assert artifact_6.par_2 == "direct-value"

    def should_inject_direct_values_in_args(self):
        injector = self.given_a_dependency_injector()

        artifact_7 = injector.construct("artifact_7")
        assert artifact_7.par_a == 1
        assert artifact_7.par_b == "direct-value"

    def should_inject_as_factory(self):
        injector = self.given_a_dependency_injector()
        artifact_8 = injector.construct("artifact_8")
        artifact_2 = injector.construct("artifact_2")

        assert artifact_2.par_1 != artifact_8.par_1
        assert type(artifact_8.par_1) is Artifact1
